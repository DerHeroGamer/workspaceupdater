package com.rocobyte.util;

import com.rocobyte.WorkspaceUpdater;

import java.io.File;

/**
 * Created by Robin Knobloch on 29/11/2019
 * © Copyright 2019 by RocoByte.com - All rights reserved.
 **/

public class FileUtil {

    public static void createFolder(String pathname, String filename){
        if(folderExist( pathname, filename )){
            return;
        }
        System.out.println( WorkspaceUpdater.SHORT_PREFIX + "Created folder " + pathname + "/" + filename );
        new File( new File(pathname).getAbsolutePath() + "/" + filename ).mkdir();
    }

    public static boolean folderExist(String pathname, String filename){
        return new File( new File(pathname).getAbsolutePath() + "/" + filename ).exists();
    }

    public static void deleteFolder(String pathname, String filename){
        if(!folderExist( pathname, filename )){
            return;
        }
        System.out.println( "Deleted folder " + filename );
        new File( new File(pathname).getAbsolutePath() + "/" + filename ).delete();
    }

}
