package com.rocobyte.util;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Created by Robin Knobloch on 29/11/2019
 * © Copyright 2019 by RocoByte.com - All rights reserved.
 **/

public class JsonUtil {


    public static Map<String, Object> getMap( String string ) {

        ObjectMapper mapper = new ObjectMapper();

        try {
            return mapper.readValue( string, new TypeReference<HashMap<String, Object>>() {
            } );
        } catch ( IOException e ) {
            System.out.println( "Error:" + e.getMessage() );
            return new HashMap<String, Object>();
        }

    }

    public static Map<String, Map<String, Map<String, List<String>>>> getMap( File file ) {

        ObjectMapper mapper = new ObjectMapper();

        try {
            return mapper.readValue( file, new TypeReference<HashMap<String, Object>>() {
            } );
        } catch ( IOException e ) {
            System.out.println( "Error:" + e.getMessage() );
            return null;
        }

    }


    public static void toFile( Map<String, Object> map, File file ) {

        ObjectMapper mapper = new ObjectMapper();

        try {
            mapper.writeValue( file, map );
        } catch ( Exception e ) {
            System.out.println( "Error:" + e.getMessage() );
        }
    }
}
