package com.rocobyte;

import com.rocobyte.util.FileUtil;
import com.rocobyte.util.JsonUtil;
import org.json.simple.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Scanner;
import java.util.Set;

/**
 * Created by Robin Knobloch on 29/11/2019
 * © Copyright 2019 by RocoByte.com - All rights reserved.
 **/

public class WorkspaceUpdater {

    public static String SHORT_PREFIX = "Workspace Updater ||> ";
    private static String PATH = new File( "" ).getAbsolutePath();

    private static Map<String, Map<String, Map<String, List<String>>>> organisations = new HashMap<>();

    public static void main( String[] args ) {

        System.out.println( " " );
        System.out.println( " __          __        _                               _    _           _       _            \n" +
                " \\ \\        / /       | |                             | |  | |         | |     | |           \n" +
                "  \\ \\  /\\  / /__  _ __| | _____ _ __   __ _  ___ ___  | |  | |_ __   __| | __ _| |_ ___ _ __ \n" +
                "   \\ \\/  \\/ / _ \\| '__| |/ / __| '_ \\ / _` |/ __/ _ \\ | |  | | '_ \\ / _` |/ _` | __/ _ \\ '__|\n" +
                "    \\  /\\  / (_) | |  |   <\\__ \\ |_) | (_| | (_|  __/ | |__| | |_) | (_| | (_| | ||  __/ |   \n" +
                "     \\/  \\/ \\___/|_|  |_|\\_\\___/ .__/ \\__,_|\\___\\___|  \\____/| .__/ \\__,_|\\__,_|\\__\\___|_|   \n" +
                "                               | |                           | |                             \n" +
                "                               |_|                           |_|                             " );
        System.out.println( " " );
        System.out.println( "By RocoByte.com - All rights reserved" );
        System.out.println( " " );
        handleStartup();
    }


    private static void handleStartup(){
        Scanner scanner = new Scanner(System.in);
        int input;

        System.out.println( "Welcome to the Workspace-Updater!" );

        System.out.println( " " );
        System.out.println( "[1] Download projects" );
        System.out.println( "[2] Update projects" );
        System.out.println( " " );
        System.out.println( "Please enter a number to continue." );

        input = scanner.nextInt();

        switch ( input ){
            case 1:
                System.out.println(SHORT_PREFIX + "Starting download process.." );
                start();
                break;
            case 2:
                System.out.println(SHORT_PREFIX + "Starting update process.." );
                handleUpdate();
                break;
        }
    }

    private static void start() {
        System.out.println(SHORT_PREFIX + "Used path: " + PATH );
        System.out.println(SHORT_PREFIX + "Still waiting for main startup to complete..." );
        organisations = JsonUtil.getMap(new File("config.json"));

        createAndDownload();
        System.out.println(SHORT_PREFIX + "Yeah! Your Workspace was updated successfully!" );
    }

    private static void handleUpdate(){
        update( "PUBLIC" );
    }

    private static void update(String state) {
        System.out.println(SHORT_PREFIX +  "Please, wait a moment, searching for updates with state " + state );
        getOrganisations().forEach( organisations -> {

            getCategories( organisations ).forEach( categories -> {
                getStates( organisations, categories ).forEach( states -> {

                    if(states.equalsIgnoreCase( state )){
                        Arrays.asList( Objects.requireNonNull(
                                new File( PATH + "/" + organisations + "/" + categories + "/" + states )
                                        .listFiles( File::isDirectory ) )
                        ).forEach( file -> {
                            runShellCommand( "git pull", "" + file );
                        } );
                    }
                } );
            } );
        } );

        System.out.println( SHORT_PREFIX + "Update done." );
    }

    private static void createAndDownload() {
        System.out.println(SHORT_PREFIX + "Please, wait a moment, the projects are checked and downloaded.." );
        getOrganisations().forEach( organisations -> {
            FileUtil.createFolder( PATH, organisations );
            getCategories( organisations ).forEach( categories -> {
                FileUtil.createFolder( PATH + "/" + organisations, categories );

                getStates( organisations, categories ).forEach( states -> {
                    FileUtil.createFolder( PATH + "/" + organisations + "/" + categories, states );

                    getCommands( organisations, categories, states ).forEach( command -> {
                        System.out.println( SHORT_PREFIX + "Checking and downloading for '" + command + "'.." );
                        runShellCommand( command, PATH + "/" + organisations + "/" + categories + "/" + states + "/" );
                    } );

                } );
            } );
        } );
    }

    private static Set<String> getOrganisations() {
        return organisations.keySet();
    }

    private static Map<String, List<String>> getCategory( String organisation, String category ) {
        return organisations.get( organisation ).get( category );
    }

    private static Set<String> getCategories( String organisation ) {
        return organisations.get( organisation ).keySet();
    }

    private static Set<String> getStates( String organisation, String category ) {
        return organisations.get( organisation ).get( category ).keySet();
    }

    private static List<String> getCommands( String organisation, String category, String state ) {
        Map<String, List<String>> map = getCategory( organisation, category );
        return map.get( state );
    }

    public static String runShellCommand( String command, String path ) {
        StringBuilder output = new StringBuilder();
        Process process;

        try {

            if ( path != null) {
                process = Runtime.getRuntime().exec( command, null, new File( path ) );
            } else {
                process = Runtime.getRuntime().exec( command );
            }
        } catch ( IOException e ) {
            e.printStackTrace();
            return null;
        }

        BufferedReader standardOut = new BufferedReader( new InputStreamReader( process.getInputStream() ) );
        String line;
        try {
            while ( ( line = standardOut.readLine() ) != null ) {
                output.append( line ).append( System.lineSeparator() );
            }
        } catch ( IOException e ) {
            e.printStackTrace();
        }

        return output.toString();
    }
}
